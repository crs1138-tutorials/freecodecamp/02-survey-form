# Changelog
All notable changes to the **02 Survey Form** will be documented in this file. I'm using [semantic versioning](https://semver.org/) and the structure of this changelog is based on [keepachangelog.com](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [1.0.0] - 2019-05-12

### Added
* Creates overall structure of the page
* Adds title, short description
* adds required text field for name
* adds required email field
* adds number field for price
* adds multiple option select box for user to choose the favourite medium
* adds radio button group where user can choose one of the motives
* adds checkboxes to select none, one or many different media
* adds textarea for additional comments
* adds submit button
